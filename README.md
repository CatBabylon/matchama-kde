<p align="center">
  <img src="https://gitlab.com/cscs/matchama-kde/raw/master/preview.png" alt="Preview Matchama-Dark KDE"/>
  <sup><sub> Color Palette: Matchama Dark | Aurorae decoration: Matchama Dark | Plasma Theme: Matchama Dark | Icons: <a href="https://github.com/PapirusDevelopmentTeam/papirus-icon-theme">Papirus-Dark-Maia</a></sub></sup>
</p>

Matchama KDE - This is a port of the popular [GTK theme Matcha](https://github.com/NicoHood/Matcha-theme) for Plasma 5 desktop with a few additions and extras.

In this repository you'll find:

- Aurorae Themes
- Konsole Color Schemes
- Konversation Themes
- Kvantum Themes
- Plasma Color Schemes
- Plasma Desktop Themes
- Plasma Look-and-Feel Settings
- Wallpapers
- Yakuake Skins

## Installation

### Matchama KDE Installer

Use the scripts to install the latest version directly from this repo (independently on your distro):

**NOTE:** Use the same script to update Matchama-KDE.

#### Install

```
curl -O https://gitlab.com/cscs/matchama-kde/raw/master/install.sh
chmod +x install.sh
./install.sh
```

#### Uninstall

```
curl -O https://gitlab.com/cscs/matchama-kde/raw/master/install.sh
chmod +x install.sh
env uninstall=true ./install.sh
```

### Donate  

Everything is free, but you can donate using these:  

<a href='https://ko-fi.com/X8X0VXZU' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a> &nbsp; <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52'><img height='36' style='border:0px;height:36px;' src='https://gitlab.com/cscs/resources/raw/master/paypalkofi.png' border='0' alt='Donate with Paypal' />  

## License

GNU GPL v3